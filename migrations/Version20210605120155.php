<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210605120155 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE http_log_seq CASCADE');
        $this->addSql('CREATE TABLE http_log (id SERIAL NOT NULL, url VARCHAR(2083) NOT NULL, request JSON NOT NULL, response JSON DEFAULT NULL, response_http_code CHAR(3) DEFAULT NULL, ip_address inet DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE http_log_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE http_log');
    }
}
