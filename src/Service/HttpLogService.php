<?php

declare(strict_types=1);

namespace App\Service;

use App\Contract\Service\HttpLogServiceContract;
use App\Exception\ValidationException;
use App\Repository\HttpLogRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class HttpLogService implements HttpLogServiceContract
{
    /** @var HttpLogRepository */
    private HttpLogRepository $repository;

    /** @var ValidatorInterface */
    private ValidatorInterface $validator;

    public function __construct(HttpLogRepository $repository, ValidatorInterface $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function list(?string $ipAddress = null): array
    {
        if ($ipAddress) {
            $errors = $this->validator->validate($ipAddress, new Assert\Ip);

            if ($errors->count()) {
                throw new ValidationException((array) $errors);
            }
        }

        return $this->repository->findByIp($ipAddress);
    }

    public function writeRequestLog(Request $request): ?int
    {
        $data = [];
        $data['requestData']['headers'] = $this->prepareHeaders($request->headers->all());
        $data['requestData']['content'] = $request->getContent();
        $data['url'] = $request->getSchemeAndHttpHost() . $request->getRequestUri();
        $data['ipAddress'] = $request->getClientIp();

        return $this->repository->insertOrUpdate($data);
    }

    public function writeResponseLog(int $httpLogId, Response $response): ?int
    {
        $data = [];
        $data['id'] = $httpLogId;
        $data['responseData']['headers'] = $this->prepareHeaders($response->headers->all());
        $data['responseData']['content'] = $response->getContent();
        $data['responseHttpCode'] = (string) $response->getStatusCode();

        return $this->repository->insertOrUpdate($data);
    }

    private function prepareHeaders(array $headersArray): array
    {
        return array_map(fn($valueArr) => $valueArr[0], $headersArray);
    }
}
