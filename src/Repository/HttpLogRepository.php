<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\HttpLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HttpLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method HttpLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method HttpLog[]    findAll()
 * @method HttpLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class HttpLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HttpLog::class);
    }

    /**
     * @param string|null
     * @return array
     */
    public function findByIp(?string $ipAddress = null): array
    {
        $q = $this->createQueryBuilder('l');

        if ($ipAddress) {
            $q->andWhere('l.ip_address = :ip')
                ->setParameter('ip', $ipAddress);
        }

        return $q->getQuery()->getArrayResult();
    }

    /**
     * @param int $id
     * @return HttpLog|null
     */
    public function findOneById(int $id): ?HttpLog
    {
        return $this->findOneBy([
            'id' => $id
        ]);
    }

    /**
     * @param array $fields
     * @return int|null
     */
    public function insertOrUpdate(array $fields): ?int
    {
        $em = $this->getEntityManager();
        $entity = $this->getEntityName();

        $id = $fields['id'] ?? null;
        $httpLog = $id ? $this->findOneById($id) : new $entity;

        try {
            if (!empty($fields['requestData'])) {
                $httpLog->setRequest($fields['requestData']);
            }

            if (!empty($fields['url'])) {
                $httpLog->setUrl($fields['url']);
            }

            if (!empty($fields['ipAddress'])) {
                $httpLog->setIpAddress($fields['ipAddress']);
            }

            if (!empty($fields['responseData'])) {
                $httpLog->setResponse($fields['responseData']);
            }

            if (!empty($fields['responseHttpCode'])) {
                $httpLog->setResponseHttpCode($fields['responseHttpCode']);
            }

            $em->persist($httpLog);
            $em->flush();

            return $httpLog->getId();
        } catch (\Exception $e) {
            return null;
        }
    }
}
