<?php

declare(strict_types=1);

namespace App\EventListener\HttpLog;

use App\Contract\Service\HttpLogServiceContract;
use Symfony\Component\HttpKernel\Event\RequestEvent;

final class RequestListener
{
    /** @var HttpLogServiceContract */
    private HttpLogServiceContract $httpLogService;

    private string $httpLogHeader;

    private bool $httpLogEnabled;

    public function __construct(
        HttpLogServiceContract $httpLogService,
        bool $httpLogEnabled,
        string $httpLogHeader
    ) {
        $this->httpLogService = $httpLogService;
        $this->httpLogEnabled = $httpLogEnabled;
        $this->httpLogHeader = $httpLogHeader;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$this->httpLogEnabled) {
            return;
        }

        $request = $event->getRequest();
        $hasHttpLogHeader = (bool) $request->headers->has($this->httpLogHeader);

        if ($hasHttpLogHeader && $event->isMainRequest()) {
            $httpLogId = $this->httpLogService->writeRequestLog($request);
            $request->request->set('httpLogId', $httpLogId);
        }
    }
}
