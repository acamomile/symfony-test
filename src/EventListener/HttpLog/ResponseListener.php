<?php

declare(strict_types=1);

namespace App\EventListener\HttpLog;

use App\Contract\Service\HttpLogServiceContract;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

final class ResponseListener
{
    /** @var HttpLogServiceContract */
    private HttpLogServiceContract $httpLogService;

    private string $httpLogHeader;

    private bool $httpLogEnabled;

    public function __construct(
        HttpLogServiceContract $httpLogService,
        bool $httpLogEnabled,
        string $httpLogHeader
    ) {
        $this->httpLogService = $httpLogService;
        $this->httpLogEnabled = $httpLogEnabled;
        $this->httpLogHeader = $httpLogHeader;
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        if (!$this->httpLogEnabled) {
            return;
        }

        $request = $event->getRequest();
        $hasHttpLogHeader = (bool) $request->headers->has($this->httpLogHeader);
        $httpLogId = (int) $request->request->get('httpLogId');

        if ($hasHttpLogHeader && $httpLogId) {
            $this->httpLogService->writeResponseLog($httpLogId, $event->getResponse());
        }
    }
}
