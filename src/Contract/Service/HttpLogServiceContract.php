<?php

namespace App\Contract\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface HttpLogServiceContract
{
    public function writeRequestLog(Request $request): ?int;

    public function writeResponseLog(int $httpLogId, Response $response): ?int;

    public function list(?string $ipAddress = null): array;
}
