<?php

namespace App\Controller\Admin;

use App\Exception\ValidationException;
use App\Contract\Service\HttpLogServiceContract;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HttpLogController extends AbstractController
{
    /**
     * @Route("/admin/http-logs", name="http-logs", methods={"GET"})
     */

    public function index(Request $request, HttpLogServiceContract $httpLogService): Response
    {
        $logsList = [];
        try {
            $logsList = $httpLogService->list($request->get('ip_address'));
        } catch (ValidationException $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('/admin/http_logs/index.html.twig', [
            'logsList' => $logsList,
        ]);
    }
}
